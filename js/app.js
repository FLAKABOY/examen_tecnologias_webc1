document.addEventListener('DOMContentLoaded', function () {
    const imagesDiv = document.getElementById('images');
    const label = document.getElementById('lblC');

    const btnBuscar = document.getElementById('btnBuscar');

    btnBuscar.addEventListener('click', function () {
        const selectedValue = document.querySelector('input[name="contenido"]:checked').value;

        fetch(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${selectedValue}`)
            .then(response => response.json())
            .then(data => {
                // Limpiar div de imágenes
                imagesDiv.innerHTML = '';

                // Obtener todos los cócteles
                const allCocktails = data.drinks;

                // Crear filas de 4 cócteles
                for (let i = 0; i < allCocktails.length; i += 4) {
                    const rowCocktails = allCocktails.slice(i, i + 4);

                    // Crear un contenedor para la fila
                    const rowContainer = document.createElement('div');
                    rowContainer.classList.add('row-container');

                    // Iterar sobre los cócteles y mostrar la información
                    rowCocktails.forEach(cocktail => {
                        const cocktailName = cocktail.strDrink;
                        const cocktailImage = cocktail.strDrinkThumb;

                        // Crear un contenedor para cada cóctel
                        const cocktailContainer = document.createElement('div');
                        cocktailContainer.classList.add('cocktail-container');

                        // Crear la imagen
                        const cocktailImg = document.createElement('img');
                        cocktailImg.classList.add('cocktail-image');
                        cocktailImg.src = cocktailImage;
                        cocktailImg.alt = cocktailName;

                        // Crear el título
                        const cocktailTitle = document.createElement('p');
                        cocktailTitle.classList.add('cocktail-title');
                        cocktailTitle.textContent = cocktailName;

                        // Agregar la imagen y el título al contenedor del cóctel
                        cocktailContainer.appendChild(cocktailImg);
                        cocktailContainer.appendChild(cocktailTitle);

                        // Agregar el contenedor al div de imágenes
                        rowContainer.appendChild(cocktailContainer);
                    });

                    // Agregar la fila al contenedor principal
                    imagesDiv.appendChild(rowContainer);
                }

                // Actualizar el total de cócteles
                label.textContent = `Total de cocteles: ${allCocktails.length}`;
            })
            .catch(error => console.error('Error al obtener datos de la API:', error));
    });

    btnLimpiarCoctel.addEventListener('click', function () {
        // Limpiar div de imágenes
        imagesDiv.innerHTML = '';
        label.textContent = 'Total de cocteles';
    });
});
